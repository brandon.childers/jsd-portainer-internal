FROM portainer/portainer
LABEL version="1.0"
LABEL maintainer="brandon.childers@juabsd.org"
LABEL is-production="false"
EXPOSE 9000/tcp
